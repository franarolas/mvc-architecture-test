import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class LoginController extends ControllerMVC {
  /// Singleton
  static LoginController _this;
  static LoginController get con => _this;
  LoginController._();
  factory LoginController() {
    if (_this == null) _this = LoginController._();
    return _this;
  }

  /// Private variables
  var _formKey = GlobalKey<FormState>();
  var _userFieldController = TextEditingController();
  var _passwordFieldController = TextEditingController();

  /// Functions
  login(BuildContext context){
    String user, password;
    if(_formKey.currentState.validate()){
      snackBar(context, 'Processing data');
      user = _userFieldController.text.toLowerCase();
      password = _passwordFieldController.text;
      print('User: $user; Pass: $password');
      Future.delayed(Duration(seconds: 1));
      if(user == 'Test' && password == 'pw'){
        Navigator.popAndPushNamed(context, '/HomePage');
      } else {
        snackBar(context, 'Failed login');
      }
    }
  }

  snackBar(BuildContext context, String text){
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(text),));
  }

  /// Getters/Setters
  get formKey => _formKey;
  get userFieldController => _userFieldController;
  get passwordFieldController => _passwordFieldController;
}