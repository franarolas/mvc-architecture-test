import 'package:mvc_pattern/mvc_pattern.dart';

class HomeController extends ControllerMVC {
  /// Singleton
  static HomeController _this;
  static HomeController get con => _this;
  HomeController._();
  factory HomeController() {
    if (_this == null) _this = HomeController._();
    return _this;
  }

  /// Variables
  int _counter = 0;

  /// Functions
  void incrementCounter() => setState(() {
    _counter++;
  });

  /// Getters and setters
  get counter => _counter;
}