import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:mvc_test/controllers/login_controller.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @protected
  @override
  createState() => _LoginPageState();
}

class _LoginPageState extends StateMVC<LoginPage> {
  LoginController _con;

  _LoginPageState() : super(LoginController());

  @override
  void initState() {
    super.initState();

    _con = LoginController.con;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Using a static reference to set our appbar title.
          title: Text('Test mvc'),
        ),
        body: Builder(
          builder: (context) => Form(
            key: _con.formKey,
            child: Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: _con.userFieldController,
                    decoration: InputDecoration(labelText: 'Username(test)'),
                  ),
                  TextFormField(
                    controller: _con.passwordFieldController,
                    decoration: InputDecoration(labelText: 'Password(pw)'),
                  ),
                  RaisedButton(
                    child: Text('Login'),
                    onPressed: () => _con.login(context),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
