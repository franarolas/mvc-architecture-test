import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:mvc_test/controllers/home_controller.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);
  @protected
  @override
  createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage> {
  HomeController _con;

  _HomePageState() : super(HomeController());

  @override
  void initState() {
    super.initState();

   _con = HomeController.con;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Using a static reference to set our appbar title.
        title: Text('Test mvc'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'teesttt',
            ),
            Text(
              '${_con.counter}',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _con.incrementCounter();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}